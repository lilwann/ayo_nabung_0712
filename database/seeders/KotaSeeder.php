<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_kelurahan_kota = [
        	[
        		'nama' => 'Sleman',
        		'kecamatan' => [
        			[
        				'nama' => 'Depok',
        				'kelurahans' => ['Caturtunggal', 'Condongcatur', 'Maguwoharjo']
        			],
        			[
        				'nama' => 'Gamping',
        				'kelurahans' => ['Ambarketawang', 'Nogotirto', 'Trihanggo']
        			],
        		]
        	],
        	[
        		'nama' => 'Yogyakarta (Kota)',
        		'kecamatan' => [
        			[
        				'nama' => 'Danurejan',
        				'kelurahans' => ['Bausasran', 'Tegalpanggung', 'Suryatmajan']
        			]
        		]

        	],
        	[
        		'nama' => 'Kulon Progo',
        		'kecamatan' => [
        			[
        				'nama' => 'Pengasih',
        				'kelurahans' => ['Karangsari', 'Kedungsari', 'Margosari']
        			],
        		]

        	],
        	[
        		'nama' => 'Bantul',
        		'kecamatan' => [
        			[
        				'nama' => 'Banguntapan',
        				'kelurahans' => ['Banguntapan', 'Baturetno', 'Jagalan', 'Jambidan', 'Potorono', 'Singosaren']
        			],
        			[
        				'nama' => 'Pajangan',
        				'kelurahans' => ['Guwosari', 'Sendangsari', 'Triwidadi']
        			],
        		]

        	],
        	[
        		'nama' => 'Gunungkidul',
        		'kecamatan' => [
        			[
        				'nama' => 'Wonosari',
        				'kelurahans' => ['Gari', 'Karangtengah', 'Karangrejek', 'KepekMulo', 'Piyaman', 'Pulutan', 'Selang', 'Siraman', 'Wareng', 'Wonosari', 'Wunung']
        			],
        		]

        	],
        ];

        foreach ($arr_kelurahan_kota as $key => $value) {
        	$id_kota = DB::table('kota')->insertGetId([
	            'nama_kota' => $value['nama'],
	        ]);

	        foreach ($value['kecamatan'] as $key2 => $value2) {
	        	$id_kecamatan = DB::table('kecamatan')->insertGetId([
		            'id_kota' => $id_kota,
		            'nama_kecamatan' => $value2['nama'],
		        ]);

		        foreach ($value2['kelurahans'] as $key3 => $value3) {
		        	DB::table('kelurahans')->insert([
				        'id_kecamatan' => $id_kecamatan,
				        'nama_kelurahan' => $value3,
				    ]);
		        }
	        }
        }

        

        

        
    }
}
