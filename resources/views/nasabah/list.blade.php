<div class="row">                   
  <div class="col-md-12">
    <div class="card">
      <div class="header">
        <h4 class="title">List Nasabah Bank</h4>
        <p class="category">
          Ini adalah daftar nasabah yang sudah diinput, <a href="#">Klik Disini Untuk Input Baru</a>
        </p>
      </div>
      <div class="content table-responsive table-full-width">
        <table class="table table-hover table-striped" id="table-data">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Alamat Kelurahan</th>
              <th>Jenis Rekening</th>
              <th>No. rekening</th>
              <th>Saldo</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($list as $data)
              <tr>
                <td>
                  {{ $data->nama }}
                </td>
                <td>
                  {{ $data->kelurahan }}, {{ $data->kecamatan }} {{ $data->kota }}
                </td>
                <td>
                  {{ $data->jenis }}
                </td>
                <td>
                  {{ $data->nomor_rek }}
                </td>
                <td>
                  Rp {{ $data->saldo }}
                </td>
                <td>
                  <a href="">Edit</a> | 
                  <a href="">Hapus</a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>  

</div>